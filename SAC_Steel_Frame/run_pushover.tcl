##############################################################################################################
# Reagan Chandramohan                                                                                        #
# John A. Blume Earthquake Engineering Center                                                                #
# Stanford University                                                                                        #
# Last edited: 02-Jun-2015
##############################################################################################################

# Run a pushover analysis on a steel moment frame built using the CreateSteelMFModel procedure, employing a
# lateral load profile proportional to the mass matrix times the first mode shape.

##############################################################################################################

# Build the model
source constants_units_kip_in.tcl
source frame_data.tcl
source create_steel_mf_model.tcl
CreateSteelMFModel frame_data.tcl

# Define the output directory 
set recorderdir Analysis_Results/Pushover
file mkdir $recorderdir

# Define the story drift recorders at the leaning column
set lc_bay [expr {$num_bays + 1}]
for {set story [expr {$num_basement_levels + 1}]} {$story <= $num_stories} {incr story} {
    if {$story == 1} {
        set nodei 1[format "%02d" $lc_bay]002
    } else {
        set nodei 1[format "%02d" $lc_bay][format "%02d" [expr {$story - 1}]]4
    }
    set nodej 1[format "%02d" $lc_bay][format "%02d" $story]4
    recorder Drift -file $recorderdir/story${story}_drift.out -time -iNode $nodei -jNode $nodej -dof 1 \
            -perpDirn 2
}

# Define the roof drift recorder at the leaning column
if {$basement_flag} {
    set ground_node 1[format "%02d" $lc_bay][format "%02d" $num_basement_levels]4
} else {
    set ground_node 1[format "%02d" $lc_bay]002
}
recorder Drift -file $recorderdir/roof_drift.out -time -iNode $ground_node -jNode \
        1[format "%02d" $lc_bay][format "%02d" $num_stories]4 -dof 1 perpDirn 2

# Compute the product of the mass matrix and first mode shape to be used as the lateral load pattern.
# Maintain a sum of the displacements at all stories.
set load_profile_sum 0.0
for {set story [expr {$num_basement_levels + 1}]} {$story <= $num_stories} {incr story} {
    set story_mass [lindex $story_masses [expr {$story - $num_basement_levels - 1}]]
    set story_disp [nodeEigenvector 100[format "%02d" $story]3 1 1]
    set load_profile($story) [expr {$story_mass*$story_disp}]
    set load_profile_sum [expr {$load_profile_sum + $load_profile($story)}]
}

# Define the pushover load pattern such that the sum of all lateral loads equals 1. This allows interpretation
# of the time step of the analysis as the applied load ratio.
set pushover_load_ts 2
timeSeries Linear $pushover_load_ts

set pushover_load_pattern 2
pattern Plain $pushover_load_pattern $pushover_load_ts {
    for {set story [expr {$num_basement_levels + 1}]} {$story <= $num_stories} {incr story} {
        load 100[format "%02d" $story]3 [expr {$load_profile($story)/$load_profile_sum}] 0.0 0.0
    }
}

# Compute the target roof displacement
set target_roof_drift 0.04

set roof_height 0.0
foreach story_height [lrange $story_heights $num_basement_levels end] {
    set roof_height [expr {$roof_height + $story_height}]
}
set target_roof_disp [expr {$target_roof_drift*$roof_height}]

# Initialize the analysis parameters
set numsteps 2000
set tol 1e-10
set maxiter 50
set print_flag 0

constraints Transformation
numberer RCM
system SparseGEN
test RelativeEnergyIncr $tol $maxiter $print_flag
algorithm Newton
integrator DisplacementControl 1[format "%02d" $lc_bay][format "%02d" $num_stories]4 1 \
        [expr {$target_roof_disp/$numsteps}]
analysis Static

# Run the pushover analysis
if {[analyze $numsteps]} {
    puts "Pushover failed"
} else {
    puts "Pushover complete"
}

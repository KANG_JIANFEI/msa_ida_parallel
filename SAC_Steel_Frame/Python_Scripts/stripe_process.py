# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import os
import shelve, contextlib
import re
import scipy.stats, scipy.optimize

# Define directories
gmdir = 'SAC_Seattle'

inpath_gm = '../../Ground_Motions'
indir_gm = os.path.join(inpath_gm, gmdir)

inpath_stripe = '../Analysis_Results/Stripe'
indir_stripe = os.path.join(inpath_stripe, gmdir)

# Define parameters
col_drift = 0.10
max_drift_plot = 0.12

# Read the elastic fundamental mode period
with open('../Model_Info/periods.out') as f:
    period = float(f.readline())

# Define the list of ground motion directories and the corresponding Sa values
stripe_dirs = ('se10in50', 'se2in50')
sa = (0.1077, 0.2453)

# Read the peak story drift values and compute the number of collapses
num_gms = []
drift = []

for stripe_dir in stripe_dirs:
    with open(os.path.join(indir_gm, stripe_dir, 'GMInfo.txt')) as f:
        gmnames = [line.split()[1][:-3] for line in f]

    num_gms.append(len(gmnames))
    drift.append([np.loadtxt(os.path.join(indir_stripe, stripe_dir, gmname, 'stripe.txt')) for gmname in \
            gmnames])

num_gms = np.array(num_gms)
drift = np.array(drift)
num_coll = np.sum(drift == col_drift, axis=1)
pcoll = num_coll.astype(float)/num_gms

# Plot Sa vs drift
mean_drift = []
for i in range(len(drift)):
    plt.scatter(drift[i], sa[i]*np.ones(len(drift[i])), s=2)
    mean_drift.append(np.mean(drift[i][drift[i] != col_drift]))
plt.plot([0] + mean_drift, [0] + list(sa), 'k')
plt.grid()
plt.xlim((0, max_drift_plot))
plt.ylim((0, np.ceil(max(sa)*10)/10))
plt.xlabel('Peak SDR ($rad$)', fontsize='x-large')
plt.ylabel('$S_a({:.2f}s)$ ($g$)'.format(period), fontsize='x-large')
plt.tight_layout()
plt.savefig(os.path.join(indir_stripe, 'Stripe.png'))
plt.show()
plt.close()

# Compute the log-likelihood of the collapse fragility curve
def loglikelihood(x, sa, num_coll, num_gms):
    prob = scipy.stats.lognorm(x[0], scale=x[1]).cdf(sa)
    likelihood = scipy.stats.binom(num_gms, prob).pmf(num_coll)
    loglikelihood = -sum(np.log(likelihood))
    return loglikelihood

# Initial guess of lognormal standard deviation and median collapse capacity respectively
x0 = (0.5, 0.4)

# Compute and plot the collapse fragility curve
result = scipy.optimize.minimize(loglikelihood, x0, args=(sa, num_coll, num_gms), method='TNC', \
        bounds=((0.05, 0.9), (0.1, 2.0)))
collcap = {'med':result.x[1], 'lnsig':result.x[0]}

factor = 10.0
limits = (collcap['med']/factor, collcap['med']*factor)
x_plot = np.logspace(*np.log10(limits), num=100)
y_plot = scipy.stats.lognorm(collcap['lnsig'], scale=collcap['med']).cdf(x_plot)
plt.semilogx(x_plot, y_plot)
plt.scatter(sa, pcoll)
plt.yticks(np.linspace(0, 1, 11))
plt.grid(which='both')
plt.xlim(*limits)
plt.ylim((0, 1))
plt.xlabel('$S_a({:.2f}s)$ ($g$)'.format(period), fontsize='x-large')
plt.ylabel('Probability of Collapse', fontsize='x-large')
plt.tight_layout()
plt.savefig(os.path.join(indir_stripe, 'Collapse_Fragility.png'))
plt.show()
plt.close()

# Save data
with contextlib.closing(shelve.open(os.path.join(indir_stripe, 'Stripe_Data.dat'))) as f:
    f['sa'] = sa
    f['drift'] = drift
    f['num_coll'] = num_coll
    f['pcoll'] = pcoll
    f['num_gms'] = num_gms
    f['collcap'] = collcap

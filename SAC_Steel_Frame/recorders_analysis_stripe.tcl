##############################################################################################################
# Reagan Chandramohan                                                                                        #
# John A. Blume Earthquake Engineering Center                                                                #
# Stanford University                                                                                        #
# Last edited: 02-Jun-2015
##############################################################################################################

# Script called from "run_stripe_mp.tcl".
# Define recorders and run one analysis.

##############################################################################################################

# Define the drift recorders
set recorderdir $outdir
for {set story 1} {$story <= $num_stories} {incr story} {
    recorder EnvelopeDrift -file $recorderdir/story${story}_drift_env.out -iNode [lindex $ctrl_nodes \
            [expr {$story - 1}]] -jNode [lindex $ctrl_nodes $story] -dof 1 -perpDirn 2
}

# Define the ground motion time series
timeSeries Path [expr {10 + $serial}] -dt $dt -filePath $indir/$filename -factor $g
set eq_load_pattern 3
pattern UniformExcitation $eq_load_pattern 1 -accel [expr {10 + $serial}]

# Define the time step used to run the analysis using the central difference scheme
set dt_factor 0.9
set periods_file [open Model_Info/periods.out]
while {[gets $periods_file line] >= 0} {set period $line}
close $periods_file
set dt_analysis [expr {$dt_factor*$period/$pi}]

# Initialize the analysis parameters and run the analysis. Check whether the structure has collapsed after
# every second and halt the analysis if it has.
constraints Transformation
numberer RCM
system SparseGEN
algorithm Linear
integrator CentralDifference
analysis Transient

set gm_length [expr {($numpts - 1)*$dt}]
set total_steps [expr {int($gm_length/$dt_analysis)}]
set steps_per_batch [expr {int(1.0/$dt_analysis)}]
set collapse_flag false

for {set steps_run 0} {$steps_run < $total_steps} {incr steps_run $steps_per_batch} {
    set fail [analyze $steps_per_batch $dt_analysis]
    if {$fail} {
        set collapse_flag true
        break
    } else {
        set max_drift [max_drift_model $ctrl_nodes]
        if {$max_drift >= $col_drift} {
            set collapse_flag true
            break
        }
    }
}

# Compute the peak story drift from the recorder files if the structure hasn't collapsed
if {!$collapse_flag} {
    set max_drift [max_drift_outfile $recorderdir $num_stories]
    if {$max_drift >= $col_drift} {
        set collapse_flag true
    }
}

# Write the analysis results to the stripe text file
if {$collapse_flag} {
    puts $stripe_file "[format "%.5f" $col_drift]"
} else {
    puts $stripe_file "[format "%.5f" $max_drift]"
}
